package com.oxylab.tomatobash;

import com.oxylab.tomatobash.frame.Game;
import com.oxylab.tomatobash.frame.Graphics;
import com.oxylab.tomatobash.frame.Screen;
import com.oxylab.tomatobash.frame.Graphics.ImageFormat;

public class LoadingScreen extends Screen {
	private float elapsed;

	public LoadingScreen(Game game) {
		
		super(game);
	}

	@Override
	public void update(float deltaTime) {
		Graphics g = game.getGraphics();
		if(elapsed==0)
			{	
		Assets.menu = g.newImage("menu.jpg", ImageFormat.RGB565);
		Assets.helptop = g.newImage("helproof.png", ImageFormat.ARGB4444);
		Assets.bg = g.newImage("woodbg.jpg", ImageFormat.RGB565);
		Assets.scoreboard = g.newImage("scoreboard.png", ImageFormat.ARGB4444);
		Assets.hourglass = g.newImage("hourg.png", ImageFormat.ARGB4444);
		Assets.addhour[0] = g.newImage("blast0.png", ImageFormat.ARGB4444);
		Assets.addhour[1] = g.newImage("blast1.png", ImageFormat.ARGB4444);
		Assets.addhour[2] = g.newImage("blast2.png", ImageFormat.ARGB4444);
		Assets.addhour[3] = g.newImage("blast3.png", ImageFormat.ARGB4444);
		Assets.addhour[4] = g.newImage("blast4.png", ImageFormat.ARGB4444);

		Assets.blast[0] = g.newImage("splash0.png", ImageFormat.ARGB4444);
		Assets.blast[1] = g.newImage("splash1.png", ImageFormat.ARGB4444);
		Assets.blast[2] = g.newImage("splash2.png", ImageFormat.ARGB4444);
		Assets.blast[3] = g.newImage("splash3.png", ImageFormat.ARGB4444);
		Assets.blast[4] = g.newImage("splash4.png", ImageFormat.ARGB4444);
		

		Assets.achiev = g.newImage("achiev.png", ImageFormat.ARGB4444);
		Assets.Controller = g.newImage("controller.png", ImageFormat.ARGB4444);
		Assets.refill = g.newImage("refillbg.png", ImageFormat.ARGB4444);
		//Assets.lboard = g.newImage("leadboard.png", ImageFormat.ARGB4444);

		Assets.sound = g.newImage("sound.png", ImageFormat.ARGB4444);
		Assets.funnyC  = g.newImage("funny/color.png", ImageFormat.ARGB4444);
		Assets.funnyR  = g.newImage("funny/red.png", ImageFormat.ARGB4444);
		//Assets.funnyS  = g.newImage("funny/shadow.png", ImageFormat.ARGB4444);
		Assets.angryC  = g.newImage("angry/color.png", ImageFormat.ARGB4444);
		Assets.angryR  = g.newImage("angry/red.png", ImageFormat.ARGB4444);
		//Assets.angryS  = g.newImage("angry/shadow.png", ImageFormat.ARGB4444);
		Assets.sickC  = g.newImage("sick/color.png", ImageFormat.ARGB4444);
		Assets.sickR  = g.newImage("sick/red.png", ImageFormat.ARGB4444);
		//Assets.sickS  = g.newImage("sick/shadow.png", ImageFormat.ARGB4444);
		Assets.happyC  = g.newImage("happy/color.png", ImageFormat.ARGB4444);
		Assets.happyR  = g.newImage("happy/red.png", ImageFormat.ARGB4444);
		//Assets.happyS  = g.newImage("happy/shadow.png", ImageFormat.ARGB4444);
		Assets.skullC  = g.newImage("skull/color.png", ImageFormat.ARGB4444);
		Assets.skullR  = g.newImage("skull/red.png", ImageFormat.ARGB4444);
		Assets.stars=g.newImage("BgTrains.png", ImageFormat.ARGB4444);
		Assets.hotfing=g.newImage("hotfing.png", ImageFormat.ARGB4444);
		Assets.hourglow=g.newImage("hourglow.png", ImageFormat.ARGB8888);

		Assets.explosion=g.newImage("explode.png", ImageFormat.ARGB4444);
		
		
		//sounds
		Assets.bash=game.getAudio().createSound("sounds/splash.ogg");
		Assets.cure=game.getAudio().createSound("sounds/magicwand.ogg");
		Assets.pop=game.getAudio().createSound("sounds/pop.ogg");
		Assets.explode=game.getAudio().createSound("sounds/explosion.ogg");
		Assets.gamova=game.getAudio().createSound("sounds/fail.ogg");
		Assets.win=game.getAudio().createSound("sounds/win.ogg");
		Assets.combos[0]=game.getAudio().createSound("sounds/1.ogg");
		Assets.combos[1]=game.getAudio().createSound("sounds/2.ogg");
		Assets.combos[2]=game.getAudio().createSound("sounds/3.ogg");
		Assets.combos[3]=game.getAudio().createSound("sounds/4.ogg");
		//Assets.laugh=game.getAudio().createSound("sounds/laugh.wav");
		Assets.wing=game.getAudio().createSound("sounds/wing.ogg");
		//Assets.explode=game.getAudio().createSound("sounds/explode.ogg");
		

		elapsed=1;	}
		//This is how you would load a sound if you had one.
		//Assets.click = game.getAudio().createSound("explode.ogg");

		if (elapsed>150)
		game.setScreen(new MainMenuScreen(game));
		//Log.i("delay","elapsed: "+elapsed+" | delay: "+deltaTime);
		elapsed+=deltaTime;

	}

	@Override
	public void paint(float deltaTime) {
		Graphics g = game.getGraphics();
		g.drawImage(Assets.splash, 0, 0);
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {

	}

	@Override
	public void backButton() {

	}
}