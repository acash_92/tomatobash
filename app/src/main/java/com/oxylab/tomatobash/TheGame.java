package com.oxylab.tomatobash;


import com.oxylab.tomatobash.frame.Screen;
import com.oxylab.tomatobash.frame.implementations.AndroidGame;


public class TheGame extends AndroidGame {

	public static String map;
	boolean firstTimeCreate = true;
	
	@Override
	public Screen getInitScreen() {

		if (firstTimeCreate) {
			//Assets.load(this);
			firstTimeCreate = false;
			}

		
		return new SplashLoadingScreen(this);

	}

	@Override
	public void onBackPressed() {
		getCurrentScreen().backButton();
		showLeadBoard=false;
	}



	@Override
	public void onResume() {
		super.onResume();

		//Assets.theme.play();

	}

	@Override
	public void onPause() {
		super.onPause();
		//Assets.theme.pause();

	}

		
	
}