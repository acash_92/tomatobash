package com.oxylab.tomatobash;


public class Heads {

	
	private int centerX = 100;
	private int centerY = 377;
	private boolean smashed = false;
	private boolean skull ;
	private boolean hot ;
	private boolean skullXplod=false;
	private double scale=0;
	
	public enum character {funny,happy,angry,sick,evil;
		 public static character getRandom() {
        return values()[(int) (Math.random() * (values().length-1))]; // evil is not randomized
    }};
	private character chr;
	
	

	public int getCenterX() {
		return centerX;
	}

	public int getCenterY() {
		return centerY;
	}

	public boolean isSmashed() {
		return smashed;
	}



	public void setCenterX(int centerX) {
		this.centerX = centerX;
	}

	public void setCenterY(int centerY) {
		this.centerY = centerY;
	}



	public void burn() {
		if(skull) skullXplod=true;
		smashed =true; 
		
	}
	
	

	public boolean isSkull() {
		return skull;
	}


	public void setSkull() {
		setCharecter(character.evil);
	}


	public character getCharecter() {
		
		return (chr);
	}


	public void setCharecter(character chr) {
		this.chr = chr;
		if (chr==character.evil){skull=true;
		hot=true;}
		else skull=false;
	}


	public boolean isHot() {
		return hot;
	}


	public void setHot() {
		this.hot = true;
		this.skull=false;
	}
public double getScale(double progress) {
	return Math.abs(1-(scale+(progress*8)%2)%2);
	}


	public boolean isSkullXplod() {
		if(skull)
		return skullXplod;
		return false;
	}

public void reset()
{centerX = 0;
  centerY = 0;
  smashed = false;
  skull=false ;
  hot=false ;
  skullXplod=false;
  chr=null;
  scale=Math.random()*2;}

	
}
