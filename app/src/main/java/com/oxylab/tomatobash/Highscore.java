package com.oxylab.tomatobash;

import com.oxylab.tomatobash.frame.Game;
import com.oxylab.tomatobash.frame.Graphics;
import com.oxylab.tomatobash.frame.Screen;
import com.oxylab.tomatobash.frame.Input.TouchEvent;

import java.util.List;

import android.content.SharedPreferences;
import android.graphics.Paint;


public class Highscore extends Screen {
	private 	timer steps;
	private int score;
	private int level;
	private int hits;
	public Highscore(Game game) {
		super(game);
		steps=new timer(null,200,10,0,0);

		SharedPreferences pref = game.getFileIO().getSharedPref();
		score=pref.getInt("highScore", 0);

		level=pref.getInt("highLevels", 0);
		hits=pref.getInt("highHits", 0);
	}
	@Override
	public void update(float deltaTime) {
		//Graphics g = game.getGraphics();
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();

		int len = touchEvents.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event = touchEvents.get(i);
			if (event.type == TouchEvent.TOUCH_UP) {

				if (inBounds(event, 40, 900, 300, 100)) {
					//high score
					game.ShowLeadboard();
					}
				if (inBounds(event, 300, 1050, 300, 100)) {
					//high score
					game.ShowAchievements();
					}
			}
			
		}
	}

	private boolean inBounds(TouchEvent event, int x, int y, int width,
			int height) {
		if (event.x > x && event.x < x + width - 1 && event.y > y
				&& event.y < y + height - 1)
			return true;
		else
			return false;
	}

	@Override
	public void paint(float deltaTime) {
		Graphics g = game.getGraphics();
		g.drawImage(Assets.bg, 0, 0);
		
		g.drawImage(Assets.Controller, 40, 900);
		drawString(
					(game.server().isSignedIn()||game.server().isConnecting())?"See":"connect to",
							310,950, 0xff838382,40,20,0xffffffff );
		drawString("Leadboard", 310,1000, 0xff838382,40,20,0xffffffff );

		g.drawImage(Assets.achiev, 570, 1050);
		drawString(
				(game.server().isSignedIn()||game.server().isConnecting())?"See":"connect to",
						440,1100, 0xff838382,40,20,0xffffffff );
	drawString("Achievements", 440,1150, 0xff838382,40,20,0xffffffff );
		
		g.drawImage(Assets.helptop, 0, 0);
		drawString("Top Score", 350, 150, 0xffffffff,100,30,0xffff8800 );
		int dx = steps.step();
		if(dx<100)drawString(""+(score*dx)/100, 350, 440, 0xff0099ff,100,25,0xffffffff );
		else {
			drawString(""+score, 350, 440, 0xff0099ff,100,25,0xffffffff );
			drawString("in", 350, 530, 0xffff9900,50,25,0xffffffff );
			
		if(dx<125){
		
			drawString(""+level+" levels", 350, 630, 0xffff88ff,70+(125-dx)*6,15,0xff000000 );
		}
		else {
			drawString(""+level+" levels", 350, 630, 0xffff88ff,70,15,0xff000000 );
			drawString("and", 350, 710, 0xffff9900,40,25,0xffffffff );
			if(dx<150);
			else if( dx<175){
				drawString(""+hits+" Hits", 350, 800, 0xff00ff77,60+(175-dx)*4,20,0xff000000 );
				}
			else {
				drawString(""+hits+" Hits", 350, 800, 0xff00ff77,60,20,0xff000000 );
				}
	}}
		}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {

	}

	@Override
	public void backButton() {
		game.setScreen(new MainMenuScreen(game));
	}
	private void drawString(String string, int i, int j,int color, int fontsize,int stroke, int strokeColor) {
		Graphics g = game.getGraphics();
		Paint paint = new Paint();
		paint.setTextSize(fontsize);
		paint.setTextAlign(Paint.Align.CENTER);
		paint.setAntiAlias(true);
		paint.setTypeface(game.getFont());
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeJoin(Paint.Join.ROUND);
		paint.setStrokeMiter(10.0f);
		paint.setStrokeWidth(stroke);
		paint.setColor(strokeColor);
		if(stroke>0)  g.drawString(string, i, j, paint);
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(color);
		
		g.drawString(string, i, j, paint);
		
		
	}
}
