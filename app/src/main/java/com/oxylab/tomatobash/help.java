package com.oxylab.tomatobash;

import com.oxylab.tomatobash.frame.Game;
import com.oxylab.tomatobash.frame.Graphics;
import com.oxylab.tomatobash.frame.Screen;
import com.oxylab.tomatobash.frame.Input.TouchEvent;

import java.util.List;

import android.graphics.Paint;
//import android.util.Log;


public class help extends Screen {
	private  int Xoff = 0,pages=3,curpage=0;
	private boolean stepping;
	private long steptime;
	public help(Game game) {
		super(game);
	}

	@Override
	public void update(float deltaTime) {
		//Graphics g = game.getGraphics();
		if( Xoff!=750*curpage)
		{int step=stepUp(75,3);
		if(step==-1) Xoff=750*curpage;
		else if( Xoff>750*curpage)Xoff=750*(curpage+1)-10*step;
		else if( Xoff<750*curpage)Xoff=750*(curpage-1)+10*step;
		}
		
		
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();

		int len = touchEvents.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event = touchEvents.get(i);
			if (event.type == TouchEvent.TOUCH_UP) {

				if (inBounds(event, 250, 1050, 250, 150)) {
					//skip to menu
					game.setScreen(new MainMenuScreen(game));
					}
				else if (curpage>0 && inBounds(event, 50, 900, 250, 150)) {
					//prev page
					curpage--;
					}
				else if (curpage<pages-1 && inBounds(event, 450, 900, 300, 150)) {
					//next page
					curpage++;
					}
				
			}
			
		}
	}

	private boolean inBounds(TouchEvent event, int x, int y, int width,
			int height) {
		if (event.x > x && event.x < x + width - 1 && event.y > y
				&& event.y < y + height - 1)
			return true;
		else
			return false;
	}
	private int stepUp(int noOfSteps,int delay)
	{
		int g=0;

	if(!stepping)
		{steptime=System.currentTimeMillis();stepping=true;}
	else {g=(int) (System.currentTimeMillis()-steptime);
		if((g/delay)>noOfSteps) stepping=false;
	}
	return stepping?(g/delay):-1;
	}
	@Override
	public void paint(float deltaTime) {
		Graphics g = game.getGraphics();
		g.drawImage(Assets.bg, 0, 0);
		g.drawImage(Assets.helptop, 0, 0);
		
		if(Xoff<750)
		{//page 1
				drawString("To complete a level",350-Xoff,300);
				drawString("bash all the tomatoes",350-Xoff,360);
				drawString("before the timer runs",350-Xoff,420);
				drawString("out.",350-Xoff,480);
				drawString("Don't touch the bombs.",350-Xoff,540);
				drawString("They do just what you",350-Xoff,600);
				drawString("thought. Kill you.",350-Xoff,660);
				drawString("...",350-Xoff,720);
		}
		 if(Xoff<2*750 && Xoff>20)
		{//page 2
				drawString("For every 200 points you ",1100-Xoff,300);
				drawString("recieve a refill potion.",1100-Xoff,360);
				drawString("The potion can refill the timer.",1100-Xoff,420);
				drawString("Use Wisely :) !",1100-Xoff,480);
				drawString("",1100-Xoff,540);
				drawString("Every red tomato ",1100-Xoff,600);
				drawString("gives you 10 points.",1100-Xoff,660);
				drawString("Don't spare any...",1100-Xoff,720);
		}
		if(Xoff<3*750 && Xoff>770)
		{//page 3
				drawString("COMBOS",1850-Xoff,300);
				drawString("(smash 2 or more identical ",1850-Xoff,360);
				drawString("tomatoes)",1850-Xoff,420);
				drawString("x2 combo - 10 x 2",1850-Xoff,480);
				drawString("x3 combo - 10 x 3",1850-Xoff,540);
				drawString("x4 combo - 10 x 4",1850-Xoff,600);
				drawString("x5 combo - 10 x 5",1850-Xoff,660);
				drawString("Bonus points if you outrun",1850-Xoff,720);
				drawString("the timer",1850-Xoff,780);
				
		}
		
		
		drawString("Skip", 375, 1150, 0xff00ff00, 100,20,0xff000000); 
		 if (curpage>0) drawString("back", 175, 1000,  0xff00ff00, 100,20,0xff000000); 
		 if (curpage<pages-1 ) drawString("next", 550, 1000,  0xff00ff00, 100,20,0xff000000); 
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {

	}

	@Override
	public void backButton() {
		game.setScreen(new MainMenuScreen(game));
	}
	private void drawString(String string, int i, int j,int color, int fontsize,int stroke, int strokeColor) {
		Graphics g = game.getGraphics();
		Paint paint = new Paint();
		paint.setTextSize(fontsize);
		paint.setTextAlign(Paint.Align.CENTER);
		paint.setAntiAlias(true);
		paint.setTypeface(game.getFont());
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeJoin(Paint.Join.ROUND);
		paint.setStrokeMiter(10.0f);
		paint.setStrokeWidth(stroke);
		paint.setColor(strokeColor);
		if(stroke>0)  g.drawString(string, i, j, paint);
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(color);
		
		g.drawString(string, i, j, paint);
		
		
	}
	private void drawString(String string, int i, int j) {
		drawString(string,i,j,0xffffffff,40,5,0xff000000);
	}
}
