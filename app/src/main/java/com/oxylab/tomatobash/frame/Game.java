package com.oxylab.tomatobash.frame;

import java.util.ArrayList;
import java.util.Map;

import playGamesHelper.GameHelper;
import android.graphics.Typeface;

public interface Game {

    public Audio getAudio();

    public Input getInput();

    public FileIO getFileIO();

    public Graphics getGraphics();

    public void setScreen(Screen screen);

    public Screen getCurrentScreen();

    public Screen getInitScreen();
    public Typeface getFont();
    
    //play games
    public boolean IsInAroom();
    public boolean IsInvited();
    public String status_w();
	public void ShowLeadboard();
	public void startQuickGame();
	public void savescore(int score);
	public void inviteppl();
	public void invinbox();

	public void broadcastScore(int score, boolean finalScore);

	Map<String, String> getDisplayNames();

	Map<String, Integer> getScores();

	ArrayList<String> getParticipants();

	void leaveRoom();

	public GameHelper server();
	public void vibrate(long duration);

	void showAdBaner(int ad);

	void ShowAchievements();

	void markAchievement(int id);

	

}
