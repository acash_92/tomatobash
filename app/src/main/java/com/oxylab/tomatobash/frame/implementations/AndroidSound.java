package com.oxylab.tomatobash.frame.implementations;

import com.oxylab.tomatobash.frame.Sound;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.SoundPool;
import android.preference.PreferenceManager;


public class AndroidSound implements Sound {
    int soundId,stream;
    SoundPool soundPool;
    Context cnx;

    public AndroidSound(SoundPool soundPool, int soundId,Context cntxt) {
        this.soundId = soundId;
        this.soundPool = soundPool;
        cnx=cntxt;
    }

    @Override
    public void play(float volume) {
    	   SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(cnx);
         	if(pref.getBoolean("soundOn", true))
         		stream=soundPool.play(soundId, volume, volume, 0, 0, 1);
    }

    @Override
    public void dispose() {
        soundPool.unload(soundId);
    }
    @Override
    public void stop() {
        soundPool.stop(stream);;
    }

}
