package com.oxylab.tomatobash.frame;

import com.oxylab.tomatobash.frame.Graphics.ImageFormat;

public interface Image {
    public int getWidth();
    public int getHeight();
    public ImageFormat getFormat();
    public void dispose();
}
