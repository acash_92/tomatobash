package com.oxylab.tomatobash.frame.implementations;

import com.oxylab.tomatobash.frame.Graphics;
import com.oxylab.tomatobash.frame.Image;

import java.io.IOException;
import java.io.InputStream;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;


public class AndroidGraphics implements Graphics {
    AssetManager assets;
    Bitmap frameBuffer;
    Canvas canvas;
    Paint paint;
    Rect srcRect = new Rect();
    Rect dstRect = new Rect();

    public AndroidGraphics(AssetManager assets, Bitmap frameBuffer) {
        this.assets = assets;
        this.frameBuffer = frameBuffer;
        this.canvas = new Canvas(frameBuffer);
        this.paint = new Paint();
    }

    @Override
    public Image newImage(String fileName, ImageFormat format) {
        Config config = null;
        if (format == ImageFormat.RGB565)
            config = Config.RGB_565;
        else if (format == ImageFormat.ARGB4444)
            config = Config.ARGB_4444;
        else
            config = Config.ARGB_8888;

        Options options = new Options();
        options.inPreferredConfig = config;
        
        
        InputStream in = null;
        Bitmap bitmap = null;
        try {
            in = assets.open(fileName);
            bitmap = BitmapFactory.decodeStream(in, null, options);
            if (bitmap == null)
                throw new RuntimeException("Couldn't load bitmap from asset '"
                        + fileName + "'");
        } catch (IOException e) {
            throw new RuntimeException("Couldn't load bitmap from asset '"
                    + fileName + "'");
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
        }

        if (bitmap.getConfig() == Config.RGB_565)
            format = ImageFormat.RGB565;
        else if (bitmap.getConfig() == Config.ARGB_4444)
            format = ImageFormat.ARGB4444;
        else
            format = ImageFormat.ARGB8888;

        return new AndroidImage(bitmap, format);
    }

    @Override
    public void clearScreen(int color) {
        canvas.drawRGB((color & 0xff0000) >> 16, (color & 0xff00) >> 8,
                (color & 0xff));
    }


    @Override
    public void drawLine(int x, int y, int x2, int y2, int color) {
        paint.setColor(color);
        canvas.drawLine(x, y, x2, y2, paint);
    }

    @Override
    public void drawRect(int x, int y, int width, int height, int color) {
        paint.setColor(color);
        paint.setStyle(Style.FILL);
        canvas.drawRect(x, y, x + width - 1, y + height - 1, paint);
    }
    
    @Override
    public void drawARGB(int a, int r, int g, int b) {
        paint.setStyle(Style.FILL);
       canvas.drawARGB(a, r, g, b);
    }
    
    @Override
    public void drawString(String text, int x, int y, Paint paint){
    	canvas.drawText(text, x, y, paint);
    	
    }
    @Override
    public void drawString90(String text, int x, int y, Paint paint){
    	canvas.save();
    	canvas.rotate(90, x, y);
    	canvas.drawText(text, x, y, paint);
    	canvas.restore();
    	
    }
    

    public void drawImage(Image Image, int x, int y, int srcX, int srcY,
            int srcWidth, int srcHeight) {
        srcRect.left = srcX;
        srcRect.top = srcY;
        srcRect.right = srcX + srcWidth;
        srcRect.bottom = srcY + srcHeight;
        
        
        dstRect.left = x;
        dstRect.top = y;
        dstRect.right = x + srcWidth;
        dstRect.bottom = y + srcHeight;

        canvas.drawBitmap(((AndroidImage) Image).bitmap, srcRect, dstRect,
                null);
    }

    @Override
    public Bitmap flush()
    {return this.frameBuffer;}

    @Override
    public void drawImage(Image Image, int x, int y) {
    	drawImage(Image,x,y,255);
         }
    
    @Override
    public void drawImage(Image image, int x, int y,int newWidth, int newHeight, float angle) {
        Matrix m=new Matrix();
        m.postTranslate(x, y);
        m.postScale(((float) newWidth) / image.getWidth(), ((float) newHeight) / image.getHeight(),x+image.getWidth()/2,y+image.getHeight()/2);
        m.postRotate(angle,x+image.getWidth()/2,y+image.getHeight()/2);
    	canvas.drawBitmap(((AndroidImage)image).bitmap, m,null);
    }
    
    @Override
    public void drawScaledImage(Image Image, int x, int y, int width, int height, int srcX, int srcY, int srcWidth, int srcHeight){
    	
    	
   	 srcRect.left = srcX;
        srcRect.top = srcY;
        srcRect.right = srcX + srcWidth;
        srcRect.bottom = srcY + srcHeight;
        
        
        dstRect.left = x;
        dstRect.top = y;
        dstRect.right = x + width;
        dstRect.bottom = y + height;
        canvas.drawBitmap(((AndroidImage) Image).bitmap, srcRect, dstRect, null);
            }
   
    @Override
    public int getWidth() {
        return frameBuffer.getWidth();
    }

    @Override
    public int getHeight() {
        return frameBuffer.getHeight();
    }

	@Override
	public void drawImage(Image Image, int x, int y, int alpha) {
		Paint alp=new Paint();
		alp.setAlpha(alpha);
		 canvas.drawBitmap(((AndroidImage)Image).bitmap, x, y,alp);
		  
	}
	@Override
	public Bitmap rotate(Image image)
	{
		Matrix matrix = new Matrix();
		Bitmap b=((AndroidImage)image).bitmap;
		matrix.postRotate(90);
		return Bitmap.createBitmap(b , 0, 0, b .getWidth(), b .getHeight(), matrix, true);
	}
}
