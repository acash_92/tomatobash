package com.oxylab.tomatobash.frame;


import android.graphics.Bitmap;
import android.graphics.Paint;

public interface Graphics {
	public static enum ImageFormat {
		ARGB8888, ARGB4444, RGB565
	}

	public Image newImage(String fileName, ImageFormat format);

	public void clearScreen(int color);

	public void drawLine(int x, int y, int x2, int y2, int color);

	public void drawRect(int x, int y, int width, int height, int color);

	public void drawImage(Image image, int x, int y, int srcX, int srcY,
			int srcWidth, int srcHeight);

	public void drawImage(Image Image, int x, int y);
	public void drawImage(Image Image, int x, int y,int alpha);
	
	public void drawString(String text, int x, int y, Paint paint);

	public int getWidth();

	public int getHeight();

	public void drawARGB(int i, int j, int k, int l);

	public void drawImage(Image Image, int x, int y, int newWidth, int newHeight,
			float angle);

	void drawScaledImage(Image Image, int x, int y, int width, int height,
			int srcX, int srcY, int srcWidth, int srcHeight);
	public Bitmap flush();

	void drawString90(String text, int x, int y, Paint paint);

	Bitmap rotate(Image image);

	}
