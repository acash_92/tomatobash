package com.oxylab.tomatobash.frame;

public interface Sound {
    public void play(float volume);

    public void dispose();
    public void stop();
}
