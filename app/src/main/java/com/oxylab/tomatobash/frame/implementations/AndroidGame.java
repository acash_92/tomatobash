package com.oxylab.tomatobash.frame.implementations;

import in.oxylab.tombash.R;
import com.oxylab.tomatobash.frame.Audio;
import com.oxylab.tomatobash.frame.FileIO;
import com.oxylab.tomatobash.frame.Game;
import com.oxylab.tomatobash.frame.Graphics;
import com.oxylab.tomatobash.frame.Input;
import com.oxylab.tomatobash.frame.Screen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import playGamesHelper.GameHelper;
import playGamesHelper.GameHelper.GameHelperListener;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Vibrator;
//import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.AdSize;
//import com.google.android.gms.ads.AdView;
//import com.google.android.gms.ads.InterstitialAd;
//import com.google.android.gms.analytics.GoogleAnalytics;
//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesActivityResultCodes;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.Multiplayer;
import com.google.android.gms.games.multiplayer.OnInvitationReceivedListener;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessageReceivedListener;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.RoomStatusUpdateListener;
import com.google.android.gms.games.multiplayer.realtime.RoomUpdateListener;

public abstract class AndroidGame extends Activity implements Game,RealTimeMessageReceivedListener,
RoomStatusUpdateListener, RoomUpdateListener, OnInvitationReceivedListener  {
    AndroidFastRenderView renderView;
    Graphics graphics;
    Audio audio;
    Input input;
    FileIO fileIO;
    Screen screen;
    WakeLock wakeLock;
	private Typeface typeface;
	GameHelper mHelper;
	protected boolean showLeadBoard=false;
	//private boolean inRoom=false;
	private String Status;
	private boolean invites=false;
	private boolean ready=false;
	private String mRoomId;
	private boolean quickstart=false;
	private boolean invitEm=false;
	private boolean respondinv=false;
	private ArrayList<Participant> participants;
	private String myId;
	
	final static int RC_SELECT_PLAYERS = 10000;
    final static int RC_INVITATION_INBOX = 10001;
    final static int RC_WAITING_ROOM = 10002;
    
    Map<String, Integer> mParticipantScore = new HashMap<String, Integer>();

    // Participants who sent us their final score.
    Set<String> mFinishedParticipants = new HashSet<String>();
	private Vibrator vibe;
//	private AdView adViewHome,adViewWoodBotom;
	private boolean showAchBoard=false;
	private int SesHighscore=0;
//	private InterstitialAd interstitial;
//	private Tracker trak;

	enum room{ empty,invited,joined,waiting,engaged};


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Log.i("debug","creating game activity");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
        int frameBufferWidth = isPortrait ? 720: 1200;
        int frameBufferHeight = isPortrait ? 1200: 700;
        Bitmap frameBuffer = Bitmap.createBitmap(frameBufferWidth,
                frameBufferHeight, Config.RGB_565);
        
        float scaleX = (float) frameBufferWidth
                / getWindowManager().getDefaultDisplay().getWidth();
        float scaleY = (float) frameBufferHeight
                / getWindowManager().getDefaultDisplay().getHeight();

        renderView = new AndroidFastRenderView(this, frameBuffer);
        graphics = new AndroidGraphics(getAssets(), frameBuffer);
        fileIO = new AndroidFileIO(this);
        audio = new AndroidAudio(this);
        input = new AndroidInput(this, renderView, scaleX, scaleY);
        screen = getInitScreen();
        typeface = Typeface.createFromAsset(getAssets(), "SHOWG.TTF");
        vibe=(Vibrator) getSystemService(VIBRATOR_SERVICE);
//        trak=GoogleAnalytics.getInstance(getApplicationContext()).newTracker(R.xml.global_tracker);//.newTracker("UA-53854044-2");
        //create relativelayout to contain renderview and ad
        RelativeLayout layout = new RelativeLayout(this);
        //Log.i("debug",""+layout.getHeight());
        layout.addView(renderView);
//        adViewHome = new AdView(this);
//        adViewHome.setAdSize(AdSize.SMART_BANNER);
//        adViewHome.setAdUnitId("ca-app-pub-6756370833535553/6395129426");
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        
//        adViewHome.setLayoutParams(lp);
//		layout.addView(adViewHome);
//        adViewHome.loadAd(new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)       // Emulator
//        	    .addTestDevice("6275EBE363474C46F663512A09528077") // My test phoned 6275EBE363474C46F663512A09528077
//
//        	    .build());
// //        botom ad
//        adViewWoodBotom = new AdView(this);
//        adViewWoodBotom.setAdSize(AdSize.SMART_BANNER);
//        adViewWoodBotom.setAdUnitId("ca-app-pub-6756370833535553/4719919828");
        RelativeLayout.LayoutParams lp2 = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        //lp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        lp2.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        //lp.setMargins(0, getWindowManager().getDefaultDisplay().getHeight(), 0, 0);
//        adViewWoodBotom.setLayoutParams(lp2);
//        layout.addView(adViewWoodBotom);
//        adViewWoodBotom.loadAd(new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)       // Emulator
//        	    .addTestDevice("6275EBE363474C46F663512A09528077") // My test phoned 6275EBE363474C46F663512A09528077
//
//        	    .build());
        setContentView(layout);
        //setContentView();
        
//        interstitial = new InterstitialAd(this);
//        interstitial.setAdUnitId("ca-app-pub-6756370833535553/3477395429");

        // Create ad request.
//        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)       // Emulator
//        	    .addTestDevice("6275EBE363474C46F663512A09528077").build();
//        interstitial.loadAd(adRequest);

        
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "oxylab:MyGame");
        
        //playgames
        mHelper = new GameHelper(this, GameHelper.CLIENT_ALL);

        // enable debug logs (if applicable)
       
            //mHelper.enableDebugLog(true, "GameHelper");
            mHelper.setConnectOnStart(fileIO.getSharedPref().getBoolean("connected", false));
           // mHelper.setMaxAutoSignInAttempts(1);

            //Log.i("debug","pref le connect on start value"+fileIO.getSharedPref().getBoolean("connected", false));
        GameHelperListener listener = new GameHelper.GameHelperListener() {
            
			@Override
            public void onSignInSucceeded() {
				fileIO.getSharedPref().edit().putBoolean("connected", true).apply();
				if(SesHighscore>0)
					savescore(SesHighscore);
            	leadboard();
            	Achboard();
            	seeInvites();
            	if(quickstart)startQuickGame();
            	if(invitEm)inviteppl();
            	if(respondinv)invinbox();
            }
            
			
			@Override
            public void onSignInFailed() {
				fileIO.getSharedPref().edit().putBoolean("connected", false).apply();
            	 }

        };
        mHelper.setup(listener);

    }

    @Override
    public void onResume() {
        super.onResume();
        wakeLock.acquire();
        screen.resume();
        renderView.resume();
    }

    @Override
    public Typeface getFont() {
    	return typeface;
    }
        @Override
        public void onPause() {
        super.onPause();
        wakeLock.release();
        renderView.pause();
        screen.pause();

        if (isFinishing())
            screen.dispose();
    }

    @Override
    public Input getInput() {
        return input;
    }

    @Override
    public FileIO getFileIO() {
        return fileIO;
    }

    @Override
    public Graphics getGraphics() {
        return graphics;
    }

    @Override
    public Audio getAudio() {
        return audio;
    }
    @Override
    public void vibrate(long duration)
    {
    	if( fileIO.getSharedPref().getBoolean("vibrate", true))
    		vibe.vibrate(duration);
    }
    @Override
    public void setScreen(Screen screen) {
        if (screen == null)
            throw new IllegalArgumentException("Screen must not be null");

        this.screen.pause();
        this.screen.dispose();
        screen.resume();
        screen.update(0);
        this.screen = screen;
        vibrate(20);
//        trak.setScreenName(screen.getClass().getSimpleName());
//        trak.send(new HitBuilders.AppViewBuilder().build());
//        trak.setScreenName(null);
    }
    
    public Screen getCurrentScreen() {

    	return screen;
    }

    private int wallSkips=0;
    @Override
    
    public void showAdBaner(final int ad)
    { 
    	runOnUiThread(new Runnable() {

		@Override
        public void run() {
//        	if(ad==3 && wallSkips>3){interstitial.show(); return;}
//        	wallSkips++;
//    	adViewWoodBotom.setVisibility(View.GONE);
//    	adViewHome.setVisibility(View.GONE);
//    	if(ad==1)adViewHome.setVisibility(View.VISIBLE);
//    	if(ad==2)adViewWoodBotom.setVisibility(View.VISIBLE);
        }});
    }
    
    //playgames requires these lifecycle methods
    @Override
    protected void onStart() {
        super.onStart();
        mHelper.onStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mHelper.onStop();
    }
    
    
    @Override
    public GameHelper server()
    {return mHelper;}
    @Override
	public void ShowLeadboard() {
    	vibe.vibrate(20);
    	showLeadBoard=true;
    	if(mHelper.isSignedIn())
    		leadboard();
    	else if(!mHelper.isConnecting() )
    	{mHelper.beginUserInitiatedSignIn();
    	}
    	 
	}    
    @Override
	public void ShowAchievements() {
    	vibe.vibrate(20);
    	showAchBoard=true;
    	if(mHelper.isSignedIn())
    		Achboard();
    	else if(!mHelper.isConnecting() )
    	{mHelper.beginUserInitiatedSignIn();
    	}
    	 
	}
    private void leadboard()
    {if(!showLeadBoard) return;
    //code to display online leadboard
    startActivityForResult(Games.Leaderboards.getLeaderboardIntent(mHelper.getApiClient(),
    		getResources().getString(R.string.leaderboard_top_scores)), 0);

    showLeadBoard=false;}
    
    private void Achboard()
    {if(!showAchBoard) return;
    //code to display online leadboard
    startActivityForResult(Games.Achievements.getAchievementsIntent(mHelper.getApiClient()), 0);

    showAchBoard=false;}
    

	public void savescore(int score)
	{ 
		if(mHelper.isSignedIn())
			{
			Games.Leaderboards.submitScore(mHelper.getApiClient(), getResources().getString(R.string.leaderboard_top_scores), score);
			SesHighscore=0;}
		else if(score>SesHighscore )
			SesHighscore= score;
	}
	
	@Override
	public void markAchievement(int id)
	{
		try{
	Games.Achievements.unlock(mHelper.getApiClient(), getResources().getString(id));
		}
		catch(Exception e){}
	}
	//realtime gaming
	@Override
	public void onRealTimeMessageReceived(RealTimeMessage rtm) {
		 byte[] buf = rtm.getMessageData();
	        String sender = rtm.getSenderParticipantId();
	        
	        if (buf[0] == 'F' || buf[0] == 'U') {
	            // score update.
	            int existingScore = mParticipantScore.containsKey(sender) ?
	                    mParticipantScore.get(sender) : 0;
	            int thisScore = (int) buf[1];
	            if (thisScore > existingScore) {
	                 mParticipantScore.put(sender, thisScore);
	            }

	            // update the scores on the screen
	            // if it's a final score, mark this participant as having finished
	            // the game
	            if ((char) buf[0] == 'F') {
	                mFinishedParticipants.add(rtm.getSenderParticipantId());
	            }
	        }
	}

	@Override
	public void onConnectedToRoom(Room b214) {
		Status="Line is OK..";
		mRoomId = b214.getRoomId();
		myId=b214.getParticipantId(Games.Players.getCurrentPlayerId(mHelper.getApiClient()));
		participants= b214.getParticipants();
	}

	@Override
	public void onDisconnectedFromRoom(Room arg0) {
		//inRoom=false;
        mRoomId = null;
        myId=null;
        participants=null;
		Status="Control.. Shit happens.";
	}

	@Override
	public void onP2PConnected(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onP2PDisconnected(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPeerDeclined(Room arg0, List<String> arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPeerInvitedToRoom(Room room, List<String> arg1) {
	participants = room.getParticipants();
		
	}

	@Override
	public void onPeerJoined(Room room, List<String> arg1) {
		participants = room.getParticipants();
			}

	@Override
	public void onPeerLeft(Room room, List<String> arg1) {
		participants = room.getParticipants();
		
	}

	@Override
	public void onPeersConnected(Room arg0, List<String> arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPeersDisconnected(Room arg0, List<String> arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onRoomAutoMatching(Room arg0) {
		Status="Haha.. I love Mash-ups";
		
	}

	@Override
	public void onRoomConnecting(Room arg0) {
		Status="Lemme make some calls..";
		
	}

	@Override
	public void onJoinedRoom(int statusCode, Room b214) {

		if (statusCode == GamesStatusCodes.STATUS_OK){ //inRoom=true;
		Status="You are in.";
		waitroom(b214);}
	}

	private void waitroom(Room b214) {
		//waiting room
				Intent i = Games.RealTimeMultiplayer.getWaitingRoomIntent(mHelper.getApiClient(), b214, 2);//min players=2
				startActivityForResult(i, RC_WAITING_ROOM);
		   	
	}

	@Override
	public void onLeftRoom(int status, String roomid) {
		Status="Ah.. You left!";
		
	}

	@Override
	public void onRoomConnected(int arg0, Room arg1) {
		Status="mmm.. Ringing";
		
	}

	@Override
	public void onRoomCreated(int statusCode, Room b214) {
		Status="Waito.";
		waitroom(b214);
	}

	@Override
	public void onInvitationReceived(Invitation inv) {
		Status="Mr. popular, You've got invitation";
		//acceptInviteToRoom(inv.getInvitationId());
		invites=true;
       // Log.i("debug","acceptInviteToRoom");
		
	}

	@Override
	public void onInvitationRemoved(String arg0) {
		invites=false;
		Status="Its rude..";
	}

	private void seeInvites() {
		Games.Invitations.registerInvitationListener(mHelper.getApiClient(), this);
		            	
		                // if we received an invite via notification, accept it; otherwise, go to main screen
		                if (mHelper.getInvitationId() != null) {
		                	invites=true;
		                	
		                }
		               
						
					}
	 public void startQuickGame() {
	        // quick-start a game with 1 randomly selected opponent
		 quickstart=false;
		 if(mHelper.isSignedIn()){
			
	        final int MIN_OPPONENTS = 1, MAX_OPPONENTS = 2;
	        Bundle autoMatchCriteria = RoomConfig.createAutoMatchCriteria(MIN_OPPONENTS,
	                MAX_OPPONENTS, 0);
	        RoomConfig.Builder rtmConfigBuilder = RoomConfig.builder(this);
	        rtmConfigBuilder.setMessageReceivedListener(this);
	        rtmConfigBuilder.setRoomStatusUpdateListener(this);
	        rtmConfigBuilder.setAutoMatchCriteria(autoMatchCriteria);
	        
	        Games.RealTimeMultiplayer.create(mHelper.getApiClient(), rtmConfigBuilder.build());}
		 else  if(!mHelper.isConnecting())
		 {mHelper.beginUserInitiatedSignIn();
			 quickstart=true;}
		 else quickstart=true;
	    }
	 
	 public void inviteppl() {
		 invitEm=false;
		 if(mHelper.isSignedIn()){Intent intent = Games.RealTimeMultiplayer.getSelectOpponentsIntent(mHelper.getApiClient(), 1, 7);
	        startActivityForResult(intent, RC_SELECT_PLAYERS);	}
	        else  if(!mHelper.isConnecting())
			 {mHelper.beginUserInitiatedSignIn();
			 invitEm=true;}
		 else invitEm=true;
		 }
	 public void invinbox() 
	 {
		 respondinv=false;
		 if(mHelper.isSignedIn()){
			 Intent intent = Games.Invitations.getInvitationInboxIntent(mHelper.getApiClient());
		        startActivityForResult(intent, RC_INVITATION_INBOX);	}
	        else  if(!mHelper.isConnecting())
			 {mHelper.beginUserInitiatedSignIn();
			 respondinv=true;}
		 else respondinv=true;
		 
		}
    public boolean IsInAroom()
    {return ready;//inRoom;
    
    }
    public boolean IsInvited()
    {return invites;}

    public String status_w()
    {String st=Status;
    Status=null;
    	return st;}
    @Override
    public void onActivityResult(int requestCode, int responseCode,
            Intent intent) {
        super.onActivityResult(requestCode, responseCode, intent);

        mHelper.onActivityResult(requestCode, responseCode, intent);
        switch (requestCode) {
            case RC_SELECT_PLAYERS:
                // we got the result from the "select players" UI -- ready to create the room
                handleSelectPlayersResult(responseCode, intent);
                break;
            case RC_INVITATION_INBOX:
                // we got the result from the "select invitation" UI (invitation inbox). We're
                // ready to accept the selected invitation:
                handleInvitationInboxResult(responseCode, intent);
                break;
            case RC_WAITING_ROOM:
                // we got the result from the "waiting room" UI.
                if (responseCode == Activity.RESULT_OK) {
                    ready=true;
                } else if (responseCode == GamesActivityResultCodes.RESULT_LEFT_ROOM) {
                    // player indicated that they want to leave the room
                    leaveRoom();
                } else if (responseCode == Activity.RESULT_CANCELED) {
                    // Dialog was cancelled (user pressed back key, for instance). In our game,
                    // this means leaving the room too. In more elaborate games, this could mean
                    // something else (like minimizing the waiting room UI).
                    leaveRoom();
                }
                break;
        }
    }
    // "Invite friends" button. We react by creating a room with those players.
    private void handleSelectPlayersResult(int response, Intent data) {
        if (response != Activity.RESULT_OK) {
            return;
        }
        // get the invitee list
        final ArrayList<String> invitees = data.getStringArrayListExtra(Games.EXTRA_PLAYER_IDS);
        // get the automatch criteria
        Bundle autoMatchCriteria = null;
        int minAutoMatchPlayers = data.getIntExtra(Multiplayer.EXTRA_MIN_AUTOMATCH_PLAYERS, 0);
        int maxAutoMatchPlayers = data.getIntExtra(Multiplayer.EXTRA_MAX_AUTOMATCH_PLAYERS, 0);
        if (minAutoMatchPlayers > 0 || maxAutoMatchPlayers > 0) {
            autoMatchCriteria = RoomConfig.createAutoMatchCriteria(
                    minAutoMatchPlayers, maxAutoMatchPlayers, 0);
             }

        // create the room
        RoomConfig.Builder rtmConfigBuilder = RoomConfig.builder(this);
        rtmConfigBuilder.addPlayersToInvite(invitees);
        rtmConfigBuilder.setMessageReceivedListener(this);
        rtmConfigBuilder.setRoomStatusUpdateListener(this);
        if (autoMatchCriteria != null) {
            rtmConfigBuilder.setAutoMatchCriteria(autoMatchCriteria);
        }
        Games.RealTimeMultiplayer.create(mHelper.getApiClient(), rtmConfigBuilder.build());
        }

    // Handle the result of the invitation inbox UI, where the player can pick an invitation
    // to accept. We react by accepting the selected invitation, if any.
    private void handleInvitationInboxResult(int response, Intent data) {
        if (response != Activity.RESULT_OK) {
                       return;
        }
        //Log.i("debug","handle invite");
       Invitation inv = data.getExtras().getParcelable(Multiplayer.EXTRA_INVITATION);
       // accept invitation
        acceptInviteToRoom(inv.getInvitationId());
    }

    // Accept the given invitation.
    void acceptInviteToRoom(String invId) {
        // accept the invitation
        RoomConfig.Builder roomConfigBuilder = RoomConfig.builder(this);
        roomConfigBuilder.setInvitationIdToAccept(invId)
                .setMessageReceivedListener(this)
                .setRoomStatusUpdateListener(this);
        //Log.i("debug","acceptInviteToRoom");
        Games.RealTimeMultiplayer.join(mHelper.getApiClient(), roomConfigBuilder.build());
    }
    // Leave the room.
    @Override
	public
    void leaveRoom() {
        ready=false;
        if (mRoomId != null) {
            Games.RealTimeMultiplayer.leave(mHelper.getApiClient(), this, mRoomId);
          
        } 
    }
    
     // Broadcast my score to everybody else.
    @Override
    public void broadcastScore(int score,boolean finalScore) {
        
        byte[] mMsgBuf=null;
		// First byte in message indicates whether it's a final score or not
        mMsgBuf[0] = (byte) (finalScore ? 'F' : 'U');

        // Second byte is the score.
        mMsgBuf[1] = (byte) score;

        // Send to every other participant.
        
        for (Participant p : participants) {
            if (p.getParticipantId().equals(myId))
                continue;
            if (p.getStatus() != Participant.STATUS_JOINED)
                continue;
              // final score notification must be sent via reliable message
                Games.RealTimeMultiplayer.sendReliableMessage(mHelper.getApiClient(), null, mMsgBuf,
                        mRoomId, p.getParticipantId());
            
        }
    }
   
    @Override
    public Map<String, String> getDisplayNames()
    {Map<String, String> dnames = new HashMap<String, String>();
    for(Participant p : participants)
    	{dnames.put(p.getParticipantId(),p.getDisplayName());}
    return dnames;
    }
    
    @Override
     public Map<String, Integer> getScores()
    {
    return mParticipantScore;
    }
    @Override
    public ArrayList<String> getParticipants()
    { ArrayList<String> ids=new ArrayList<String>();
    int lessthan=999999;
    String hiId="";
    for(Participant p : participants)
	{
    	int hiscore=0,i=0;
    	String id="";
    	for(Participant q : participants)
    	{int tmp=mParticipantScore.get(q.getParticipantId());
    		if(tmp>=hiscore && tmp<=lessthan && q.getParticipantId()!=hiId) 
    		{hiscore=tmp; id=q.getParticipantId();}
    		
    		//sort the list of ids
    	}
    	lessthan=hiscore;
		hiId=id;
    	ids.set(i++, id);
    	//Log.i("debug",id+" -> score("+hiscore);
	}
    	return ids;
    }
    }