package com.oxylab.tomatobash;

public class timer{
	public final String key;
	public final int x, y,count, delay;
	private long start;
	public timer(String key,int count,int delay, int cx, int cy)
	{this.key=key;
	this.delay=delay;
	this.count=count;
	start=System.currentTimeMillis();
	x=cx;
	y=cy;}
	public boolean isDone()
	{int g=(int) (System.currentTimeMillis()-start);
	if((g/delay)>count) return true;
	return false;}
	
	public int step()
	{
	int g=(int) (System.currentTimeMillis()-start);
		if((g/delay)>count) return count;
	return (g/delay);
	}
}