package com.oxylab.tomatobash;

import com.oxylab.tomatobash.frame.Image;
import com.oxylab.tomatobash.frame.Sound;

public class Assets {
	
	public static Image menu, splash,bg, helptop, scoreboard,hourglass;
	public static Image [] blast=  {null,null,null,null,null}, addhour=  {null,null,null,null,null} ;//,blast1,blast2,blast3,blast4;
	public static Image  sound,sickR,sickC,skullR,skullC,explosion;
	public static Image funnyR,funnyC,happyR,happyC,angryR,angryC;
	public static Sound pop,cure,explode,bash,laugh,gamova,win;
	public static Sound[] combos=new Sound[4];
	public static Image stars,hotfing,refill, hourglow, Controller,achiev;
	public static Sound wing;
	
	
}
