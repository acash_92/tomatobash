package com.oxylab.tomatobash;

import com.oxylab.tomatobash.frame.Game;
import com.oxylab.tomatobash.frame.Graphics;
import com.oxylab.tomatobash.frame.Screen;
import com.oxylab.tomatobash.frame.Input.TouchEvent;

import java.util.List;

import android.content.SharedPreferences;


public class MainMenuScreen extends Screen {
	private SharedPreferences pref;
	
	public MainMenuScreen(Game game) {
		super(game);
		pref=game.getFileIO().getSharedPref();
		game.showAdBaner(1);
	}

	@Override
	public void update(float deltaTime) {
		//Graphics g = game.getGraphics();
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		int len = touchEvents.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event = touchEvents.get(i);
			if (event.type == TouchEvent.TOUCH_UP) {

				if (inBounds(event, 65, 470, 200, 200)) {
					//high score
					game.setScreen(new Highscore(game));
					hfeedback();
					}
				else if (inBounds(event, 450, 480, 190, 190)) {
					//help
					game.setScreen(new help(game));
					hfeedback();
					Assets.bash.play(0.5f);
					
					}
				else if (inBounds(event, 30, 950, 200, 180)) {
					//sound
					SharedPreferences.Editor editor = pref.edit();
					boolean x=pref.getBoolean("soundOn", true),y=pref.getBoolean("vibrate", true);
					y=!y;
					x=y?x:!x;
					editor.putBoolean("soundOn",x );
					editor.putBoolean("vibrate",y );
					editor.apply();
					hfeedback();
					Assets.bash.play(0.5f);
					
									}
				else if (inBounds(event, 500, 930, 150, 200)) {
					//exit
					hfeedback();
					Assets.bash.play(0.5f);
					backButton();
				}
				else if (inBounds(event, 230, 680, 260, 230)) {
					//play
					//game.setScreen(new Multiplayer(game));
					game.setScreen(new GameScreen(game));
					hfeedback();
					Assets.bash.play(0.5f);
				}

			}
		}
	}

	private void hfeedback() {
		game.vibrate(15);
		Assets.bash.play(0.5f);
	}

	private boolean inBounds(TouchEvent event, int x, int y, int width,
			int height) {
		if (event.x > x && event.x < x + width - 1 && event.y > y
				&& event.y < y + height - 1)
			return true;
		else
			return false;
	}

	@Override
	public void paint(float deltaTime) {
		Graphics g = game.getGraphics();
		g.drawImage(Assets.menu, 0, 0);
		if(pref.getBoolean("soundOn", true))
		g.drawImage(Assets.sound, 38, 974,0,0,Assets.sound.getWidth(),Assets.sound.getHeight()/2);
		if(pref.getBoolean("vibrate", true))
		g.drawImage(Assets.sound, 38, 974,0,Assets.sound.getHeight()/2,Assets.sound.getWidth(),Assets.sound.getHeight());
		}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {

	}

	@Override
	public void backButton() {
        android.os.Process.killProcess(android.os.Process.myPid());

	}
}
