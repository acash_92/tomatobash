package com.oxylab.tomatobash;

import com.oxylab.tomatobash.frame.Game;
import com.oxylab.tomatobash.frame.Graphics;
import com.oxylab.tomatobash.frame.Image;
import com.oxylab.tomatobash.frame.Screen;
import com.oxylab.tomatobash.frame.Input.TouchEvent;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import android.content.SharedPreferences;
import android.graphics.Paint;
import android.util.Log;


public class Multiplayer extends Screen {
	enum GameState {
		 Running,Menu,Wait,readyGo, Timeout,Blast,GameOver,Awesome
	}

	GameState state = GameState.Wait;

	// Variable Setup

	
	private int level=1,score=0,oldscore=0,headWd=230,headHt=239;
	private long timeDump,runout;//millisec
	enum Grids{_3x3,_3x4,_4x5;
	public static Grids getRandom(int a, int b, int c) {
		double xx = Math.random();
		if(xx<((float) a)/(a+b+c)) return _3x3;
		else if(xx<((float)(a+ b))/(a+b+c)) return _3x4;
		else   return _4x5;}}

	private Grids grid=Grids._3x3;
	private ArrayList<Heads> holes;
	

	private SharedPreferences pref;

	private double levelTime;

	private int bgcolor;


	private long stTime,steptime;

	private Heads.character prevBlast;

	private int bountyHeads;

	private boolean stepping=false;

	private int timeXtnd=0;

	private int combo;
	
	private Animation refil;

	private int y=0,x=0;

	private String showscoreupdate;

	private boolean refilH;
	private timer prepTime,blastanim;
	private boolean lasttimerOut;
	private ArrayList <timer>frills=new ArrayList <timer>();

	private long pausedAt;
	private boolean paused;

	private timer statimer=new timer("",100,100,350,900),clicktimer=new timer("click",1,1,0,0);
	private double headScale()
	{switch (grid)
		{case _3x3: return 0.98;
		case _3x4: return 0.88;
		case _4x5: return 0.72;
		}
		return 0;}

	

	public Multiplayer(Game game) {
		super(game);

		// Initialize game objects here

		 holes= new ArrayList<Heads>(){{
				add(new Heads());
				add(new Heads());
				add(new Heads());
				add(new Heads());

				add(new Heads());
				add(new Heads());
				add(new Heads());
				add(new Heads());

				add(new Heads());
				add(new Heads());
				add(new Heads());
				add(new Heads());

				add(new Heads());
				add(new Heads());
				add(new Heads());
				add(new Heads());

				add(new Heads());
				add(new Heads());
				add(new Heads());
				add(new Heads());
			}};
			refil=new Animation();
			refil.addFrame(Assets.refill, 10);
			refil.addFrame(Assets.refill, 10);
			refil.addFrame(Assets.refill, 10);
			refil.addFrame(Assets.refill, 10);
			refil.addFrame(Assets.refill, 10);
			refil.addFrame(Assets.refill, 10);
		
		pref=game.getFileIO().getSharedPref();
		//sound setting
		//pref.getBoolean("soundOn", false)
		prepTime=new timer("readygo",90,20,0,0);
		
		state=GameState.Wait;
		if(game.IsInvited())game.invinbox();

	}



	private int randBg() {
		Random r = new Random();
		int i1 = r.nextInt(5);
		int [] x={0xff0000cc,0xff669900,0xff778800,0xffcc0000,0xff9933cc};
		return x[i1];
	}



	@Override
	public void update(float deltaTime) {
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
	
		if (state == GameState.Running)
			updateRunning(touchEvents, deltaTime);
		if (state == GameState.Awesome)
			showAwesome();
		if (state == GameState.Wait)
			updateWait(touchEvents);
		if (state == GameState.GameOver)
			updateGameOver(touchEvents);
		if (state == GameState.Blast)
			updateBlast(touchEvents);
		if (state == GameState.Timeout)
			updateTimeout(touchEvents);
		
		
	}

	private void nextLevel() {
		int initX=0,initY=0;
		levelTime=3000-(level*10); //gradual decrease
		bgcolor=randBg();
		runout=stTime=System.currentTimeMillis();
		combo=1;
		prevBlast=null;
		bountyHeads=0;
		//timeXtnd=1;
		//randomize
		//grid=Grids.getRandom();	//randomize
		
		//update score
		game.broadcastScore(score, false);
				int maxSkulls=3,maxHots=3; 
				if(level<3){grid=Grids._3x3;	 maxHots=3;}
				else if(level<10){grid=Grids.getRandom(1,1,0);	 maxHots=6;}
				else if(level<20){grid=Grids.getRandom(1,1,1);	 maxHots=10;}
				else if(level<30){grid=Grids.getRandom(1,2,2);	 maxHots=16;}
				else {grid=Grids.getRandom(1,4,7);	 maxHots=20;}
		if (grid==Grids._3x3){ x=3;y=3;  maxHots=maxHots>9?9:maxHots;}
		else if (grid==Grids._3x4){ x=3;y=4;  maxHots=(maxHots>12)?12:maxHots;}
		else if (grid==Grids._4x5){ x=4;y=5;			}
		maxHots=(int) (Math.random()*maxHots);
		maxSkulls=(int) (Math.random()*maxHots*0.7);
		
		initX=(int) (15+Math.round(headWd*headScale()/2));
		initY=(int) (260+Math.round(headHt*headScale()/2));
		
		
		for (int i=0;i<x*y;++i) 
		{
			Heads h = holes.get(i);
			h.reset();
			h.setCenterX((int) (initX+Math.round((i%x)*headWd*headScale())));
			h.setCenterY((int) (initY+Math.round((i/x)*headHt*headScale())));
			h.setCharecter(Heads.character.getRandom());
					
			}
		for(int i = 0,j=0;i<maxHots;++i)
		{Heads f=holes.get((int) Math.floor(Math.random()*x*y));
		if(f.isHot())i--;
		else if(i<maxSkulls)f.setSkull();
		else f.setHot(); }
		
		

		
		state = GameState.Running;
//System.gc();
	}



	private void showAwesome() {

		//if(stepUp(50,25)==-1)
			nextLevel();
	}
	private void updateBlast(List<TouchEvent> touchEvents) {
		if(blastanim.isDone())
			playGameOver();
	
	}	
	private void updateTimeout(List<TouchEvent> touchEvents) {
		Heads xz;
		int xxc=stepUp(20, 200);
		boolean popped=false;
		for(int j=xxc%2==0?x*y-1:0;xxc%2==0?j>=0:j<x*y;j+=(xxc%2==0?-1:1))
		{	xz = holes.get(j);
		if(xz.isSmashed() || xz.isSkull())continue;
		xz.setSkull(); Assets.pop.play(0.5f);
		popped=true; break;
		}
		if(xxc==-1 || popped==false)
			playGameOver();
			
	}
	
	private void updateRunning(List<TouchEvent> touchEvents, float deltaTime) {
		Heads xz ;
		TouchEvent event=null;
		long tmz = System.currentTimeMillis();
		long totLevTime = (tmz-stTime);
		timeDump=tmz-runout;
		if(timeDump>levelTime)  {state = GameState.Timeout; stepping=false; return ;}
		
		boolean levelComplete=true;
		int len = touchEvents.size();
		for (int i = 0; i < len; i++) {
			event = touchEvents.get(i);
			if (event.type == TouchEvent.TOUCH_DOWN) {		//		Log.i("debug","touch");
				if(timeXtnd>0 && inBounds(event,540,0,720,210)){
					timeXtnd--;runout=tmz; refilH=true;
					Assets.cure.play(50);
				}
			 break;
				}}

		
		for(int j=0;j<x*y;++j)
		{	xz= holes.get(j);
			if(event!=null && event.type == TouchEvent.TOUCH_DOWN){
				
				if(xz.isHot() && !xz.isSmashed() && inBoundsRound(event,xz.getCenterX(),xz.getCenterY(),Math.round(headScale()*(headWd+headHt)/4)))
			{xz.burn();	frills.add(new timer("boom", 10, 20, xz.getCenterX(), xz.getCenterY()));
			if(prevBlast==xz.getCharecter())
			{	combo++; frills.add(new timer("x"+combo, 40, 15, xz.getCenterX(), xz.getCenterY()));
			}
			else combo=1;
			
				Assets.bash.play(100);
			bountyHeads++;
			prevBlast=xz.getCharecter();
			score+=10*combo;
			}}
			if(xz.isSkullXplod()) {state = GameState.Blast;blastanim=new timer("blast",30,50,xz.getCenterX(),xz.getCenterY());
			
			}
			else if(xz.isHot() && !xz.isSmashed() && !xz.isSkull()){levelComplete=false; }
		
		}
			if(levelComplete==true){//Log.i("debug","completed level "+level);
				int bonus = (int)(5*bountyHeads*(levelTime-totLevTime)/levelTime);
				if(bonus>0)showscoreupdate="+"+bonus;
				score+=bonus;
				
			level++;
			if(totLevTime<bountyHeads*300)
			{state = GameState.Awesome; }
			else
			nextLevel();}
			if(score-(oldscore*100)>200){timeXtnd++;
			frills.add(new timer("boom", 10, 20,630,100 ));
			oldscore=score/100;}
			
	}

	private boolean inBounds(TouchEvent event, int x, int y, long width,
			long height) {
		if (event.x > x && event.x < x + width - 1 && event.y > y
				&& event.y < y + height - 1)
			return true;
		else
			return false;
	}

		private boolean inBoundsRound(TouchEvent event, int x, int y, long radius) {
			double dst = Math.pow(event.x - x ,2)+Math.pow(event.y - y ,2);
			if (dst<(radius*radius))
				return true;
			else
				return false;
		}
		private void updateWait(List<TouchEvent> touchEvents) {
		int len = touchEvents.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event = touchEvents.get(i);
			if (event.type == TouchEvent.TOUCH_UP) {
				if (inBounds(event, 100, 250, 500, 140)) {
					game.startQuickGame();
					clicktimer=new timer("click",1,3000,0,0);
					statimer=new timer("",100,100,0,0);
				}
				else if (inBounds(event, 100, 390, 500, 140)) {
					game.inviteppl();
					clicktimer=new timer("click",1,3000,0,0);
					statimer=new timer("",100,100,0,0);
				}
				else if (game.IsInvited() && inBounds(event, 100, 530, 500, 140)) {
					game.invinbox();
					clicktimer=new timer("click",1,3000,0,0);
					statimer=new timer("",100,100,0,0);
									}

				else if (inBounds(event, 100, 670, 500, 140)) {
					System.gc();
					goToMenu();	}
				//else doResume();
				if(game.IsInAroom())state=GameState.readyGo;
			}
		}
	}

	private void updateGameOver(List<TouchEvent> touchEvents) {
		int len = touchEvents.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event = touchEvents.get(i);
			if (event.type == TouchEvent.TOUCH_UP) {
				if (inBounds(event, 0, 900, 300, 100)) {
					//game.setScreen(new resultScreen(game));
					game.setScreen(new Multiplayer(game));
					//Log.e("debug","play again");
					System.gc();
					}
				else if (inBounds(event, 500, 900, 250, 100)) {
					game.setScreen(new MainMenuScreen(game));
					//Log.e("debug","menu");	
					System.gc();				
					}
				else if (inBounds(event, 200, 500, 400, 150)) {
					game.setScreen(new Highscore(game));
					//Log.e("debug","highscore");			
					System.gc();		
					}
				return;
				
			}
		}

	}



	@Override
	public void paint(float deltaTime) {
		Graphics g = game.getGraphics();
		g.drawARGB(255, (bgcolor>>4)&0xff, (bgcolor>>2)&0xff, bgcolor&0xff);
		//g.drawImage(Assets.bg1, 0, 0);
		g.drawImage(Assets.scoreboard, 0,0);
		
		if (state == GameState.Awesome)
			drawAwUI();
		if (state == GameState.Running)
			{drawRunningUI();drawfrills();}
		if (state == GameState.Wait)
			drawWaitUI();
		if (state == GameState.GameOver)
			drawGameOverUI();
		if (state == GameState.Blast)
			drawBlastUI();
		if (state == GameState.Timeout)
			drawTimeoutUI();
		if (state == GameState.readyGo)
			readyGo();
	}

	private void readyGo() {
		Graphics g = game.getGraphics();

		if(prepTime.isDone()) {
			stTime=System.currentTimeMillis()-(pausedAt-stTime);
			runout=System.currentTimeMillis()-(pausedAt-runout);
			state=GameState.Running; return; }
		int stp=prepTime.step();
		int cc=stp%(prepTime.count/3); 
		drawString(stp>60?"Go":(stp>30?"Set":"Ready"),g.getWidth()/2 , g.getHeight()/2, 0xffffffff, 50+4*cc,cc,0xff000000);
	}



	private void drawTimeoutUI() {
		Graphics g = game.getGraphics();

		drawRunningUI();
		//g.drawARGB(155, 0, 0, 0);
		drawString("Time out !!", 350, 240, 0xffffffff,80);
	}



	private void drawBlastUI() {
		Graphics g = game.getGraphics();
		int stp=blastanim.step();
		if(stp<18)
		{
			g.drawARGB(255, 255,200-stp*3,150-stp*7);
			
			g.drawScaledImage(Assets.explosion, 100, 350,500,500 ,(stp/6)*225, ((stp/3)%2)*225, 220, 220);
			
		}
		else{
			g.drawImage(Assets.helptop, 0, 0);
			
		g.drawARGB(255-(stp-18)*15, 255,150,50);
		}
		//drawString("Game Over !!", 400, 240, 0xffffffff,80);
}



	private void drawAwUI() {
		Graphics g = game.getGraphics();

		g.drawARGB(255, (bgcolor>>4)&0xff, (bgcolor>>2)&0xff, bgcolor&0xff);
		g.drawImage(Assets.stars,0, (int) (0-(.5*((System.currentTimeMillis()-steptime)%500))));
		g.drawImage(Assets.stars,0, (int) (250-(.5*((System.currentTimeMillis()-steptime)%500))));
		g.drawImage(Assets.stars,0, (int) (500-(.5*((System.currentTimeMillis()-steptime)%500))));
		g.drawImage(Assets.stars,0, (int) (750-(.5*((System.currentTimeMillis()-steptime)%500))));
		g.drawImage(Assets.stars,0, (int) (1000-(.5*((System.currentTimeMillis()-steptime)%500))));
		g.drawImage(Assets.stars,0, (int) (1250-(.5*((System.currentTimeMillis()-steptime)%500))));
		g.drawImage(Assets.stars,0, (int) (1500-(.5*((System.currentTimeMillis()-steptime)%500))));
		g.drawImage(Assets.hotfing,110, 250);
		drawString("Awesome kid.. well done", 400, 240,0xffffffff,50,10,0xff00ff00);
		
	}

	private void drawString(String string, int i, int j,int color, int fontsize,int stroke, int strokeColor) {
		Graphics g = game.getGraphics();
		Paint paint = new Paint();
		paint.setTextSize(fontsize);
		paint.setTextAlign(Paint.Align.CENTER);
		paint.setAntiAlias(true);
		paint.setTypeface(game.getFont());
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeJoin(Paint.Join.ROUND);
		paint.setStrokeMiter(10.0f);
		paint.setStrokeWidth(stroke);
		paint.setColor(strokeColor);
		if(stroke>0)  g.drawString(string, i, j, paint);
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(color);
		
		g.drawString(string, i, j, paint);
		
		
	}
	private void drawString(String string, int i, int j,int color, int fontsize) {
		drawString(string,i,j,color,fontsize,0,0);
	}


	private void drawRunningUI() {
		Image tz=null,tx=null;
		Graphics g = game.getGraphics();
		if(refilH){
			refil.update(10);
			if(refil.loop>0){refilH=false; refil.restart();
			Assets.cure.stop();
			}}
		for(int j=0;j<x*y;++j)
		{	Heads x = holes.get(j);
		double zoomsize=x.getScale(timeDump/levelTime)/6;
		int xoff = (int) ((zoomsize+headScale())*headWd),yoff=(int) ((zoomsize+headScale())*headHt);
			
		if(x.isSmashed()) continue;
		
		if(refilH){
			 g.drawImage(Assets.refill, x.getCenterX()-150, x.getCenterY()-150, 300+10*refil.getFrameCount(), 300+10*refil.getFrameCount(), 10*refil.getFrameCount());
			g.drawImage(Assets.hourglow, 537, 0,255-85*Math.abs(refil.getFrameCount()-3));
		}
		if(x.getCharecter()==(Heads.character.angry)){ tx=Assets.angryR; tz=Assets.angryC;}
		else if(x.getCharecter()==(Heads.character.funny)){ tx=Assets.funnyR; tz=Assets.funnyC;}
		else if(x.getCharecter()==(Heads.character.happy)){ tx=Assets.happyR; tz=Assets.happyC;}
		else if(x.getCharecter()==(Heads.character.sick)){ tx=Assets.sickR; tz=Assets.sickC;}
		else if(x.isSkull()){ tx=Assets.skullR; tz=Assets.skullC;}
		else {Log.e("debug",""+j);}
			if(x.isHot()){
				g.drawScaledImage(tx,
						(int) (x.getCenterX()-xoff/2),(int) (x.getCenterY()-yoff/2),xoff,yoff,
						0, 0, headWd, headHt);
				g.drawScaledImage(tz,
						(int) (x.getCenterX()-xoff/2),(int) (x.getCenterY()-yoff/2),xoff,(int)((yoff*timeDump)/levelTime),
						0, 0, headWd, (int)((headHt*timeDump)/levelTime));
					}
			else {
				g.drawScaledImage(tz,
						(int) (x.getCenterX()-xoff/2),(int) (x.getCenterY()-yoff/2),xoff,yoff,
						0, 0, headWd, headHt);
				}
		}
		drawString(""+score, 270, 200,0xffffffff,50,15,0xff000000);
		//drawString(""+level,90, 160, 0xffffffff,80,15,0xff000000);
		
		if(timeXtnd>0){g.drawImage(Assets.hourglass, 537, 0);
		drawString(""+timeXtnd, 540,200, 0xffffffff,50,15,0xff000000);
		}
		if(showscoreupdate!=null){int cc=stepUp(30,15);
		if(cc==-1)showscoreupdate=null;
		else	drawString(showscoreupdate, 300, 230-cc,0xffff0000,100,12,0xff000000);
		}
	}

	private void drawfrills() {
		Iterator<timer> loopr = frills.iterator();
		Graphics g = game.getGraphics();
		while(loopr.hasNext())
		{	timer elem = loopr.next();
			int step=elem.step();
		String key=elem.key;
		
		if(key.equals("x2") || key.equals("x3")|| key.equals("x4") || key.equals("x5")){
			drawString(key, elem.x, elem.y, randBg(), 140+3*Math.abs(elem.count/2-step),30,0xffffff00);
		}
		else if(key=="boom"){
			g.drawImage(Assets.blast[step/5],elem.x-150, elem.y-150);
		}
		
		if(elem.isDone())
		{
			loopr.remove();
		}
		}
	}
	private void drawWaitUI() {
		Graphics g = game.getGraphics();
		// Darken the entire screen so you can display the Paused screen.
		g.drawARGB(155, 0, 0, 0);
		if(!paused && clicktimer.isDone()){
		drawString("Quick Game", 350, 360, 0xffffdd00,80);
		drawString("Create Game", 350, 500,0xffffdd00,80);
		if(game.IsInvited())
		{drawString("Invitations", 350, 640,(statimer.step()/5)%2==0?0xffffffff:0xff888888,80);
		if(statimer.isDone())statimer=new timer(null,100,100,0,0);}
		else drawString("Invitations", 350, 640,0xff888888,80);
		drawString("Quit", 350, 780,0xffffdd00,80);
		}
		String status = game.status_w();
		if(status!=null)
			statimer=new timer(status,100,100,350,900);
		if(!statimer.isDone() && statimer.key!=null)
			{drawString(statimer.key, 350, 900,0xffffffff,50);
			drawString((statimer.step()/5)%2==0?" .":". ", 350, 1000,0xff00bbff,80);}
		
	}

	private void drawGameOverUI() {
		Graphics g = game.getGraphics();
		g.drawImage(Assets.helptop, 0, 0);
		drawString("GAME OVER", 350, 150, 0xffffffff,100,30,0xffff8800 );
		int dx = stepUp(175, 10);
		if(dx==-1 || lasttimerOut){dx=200; lasttimerOut=true;}
		if(dx<100)drawString("Score: "+(score*dx)/100, 350, 440, 0xffcdff88,80,25,0xff000000 );
		else {
			drawString("Score: "+score, 350, 440, 0xffcdff88,80,25,0xff000000 );
		if(dx<125){
		if(score<pref.getInt("highScore", 0))
			drawString("High Score: "+pref.getInt("highScore", 0), 350, 580, 0xffffffff,50+(125-dx)*6,15,0xff000000 );
		else
			{drawString("High Score!!", 350, 580, 0xffffffff,100+(125-dx)*6,25,0xff000000 );
			pref.edit().putInt("highScore", score).commit();
			pref.edit().putInt("highLevels", level).commit();
			}}
		else {
			if(score<pref.getInt("highScore", 0))
				drawString("High Score: "+pref.getInt("highScore", 0), 350, 580, 0xffffffff,50,15,0xff000000 );
			else
				{drawString("High Score!!", 350, 580, 0xffffffff,100,25,0xff000000 );
				pref.edit().putInt("highScore", score).commit();
				}
			if(dx<150);
			else if( dx<175){
				drawString("Play Again", 200, 940, 0xffffffff,40+(175-dx)*4,20,0xff000000 );
				drawString("Menu", 500, 940, 0xffffffff,40+(175-dx)*4,20,0xff000000 );
				}
			else {
				drawString("Play Again", 200, 940, 0xffffffff,40,20,0xff000000 );
				drawString("Menu", 500, 940, 0xffffffff,40,20,0xff000000 );
				}
	}}}

	@Override
	public void pause() {
		paused=true;
		if(state==GameState.Running)
		playGameOver();

	}

	@Override
	public void resume() {
		paused=false;
	}

	@Override
	public void dispose() {

	}

	@Override
	public void backButton() {
		if(state!=GameState.Running) goToMenu();
	}

	private void goToMenu() {
		// TODO Auto-generated method stub
		game.setScreen(new MainMenuScreen(game));

	}
private int stepUp(int noOfSteps,int delay)
{
	int g=0;

if(!stepping)
	{steptime=System.currentTimeMillis();stepping=true;}
else {g=(int) (System.currentTimeMillis()-steptime);
	if((g/delay)>noOfSteps) stepping=false;
}
return stepping?(g/delay):-1;
}
void playGameOver()
{

	game.broadcastScore(score, true);
	//game.savescore(score,level);
	state=GameState.GameOver;
	game.leaveRoom();
	}
}